# ELECTROCARDIOGRAM SIGNAL PROCESSING TEST

Electrocardiogram (ECG) signal pre-processing usually consist of several steps, being some of the most common the following:

• Recording spikes (artifacts of great amplitude) removal.
• Low-pass filtering.
• Powerline interference filtering.
• Baseline wander and offset removal. 

Using Matlab, prepare a code for ECG signal pre-processing that contains all the previous steps. The code must also detect if a given channel is not connected. 

