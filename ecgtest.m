% ELECTROCARDIOGRAM SIGNAL PROCESSING TEST
clear; close all; clc;

%% LOAD SIGNAL
load('ecgConditioningExample.mat');

%Trabajaré con la primera señal para ir realizando el preprocesado sobre
%esta misma

ECG_data = ecg(:,1); 

%% Ajuste amplitud 

G= 2000;
ecg_mv= ECG_data/G;

%% Vector de tiempo Fs

Fs=250; % Frecuencia de Nyquist (Hz)
Ts=1/Fs; %[s]

N= length(ECG_data);
vect= (1:1:N);
t= vect*Ts;

%% Centrar señal en zero

ECG_final = (ecg_mv - mean(ecg_mv))/std(ecg_mv);
figure;
subplot(2,1,1);
plot(t,ECG_final);
ylabel('Amplitud (mV)');
xlabel('Tiempo (s)');
xlim([0 10]); title('ECG Sin Fitlrar');

%% FFT 

%{
Mediante la transformada rápida de Fourier se muestra cómo la señal con
ruido cuenta con una frecuencia que se desea eliminar mediante el 
filtro paso bajo.
%}

subplot(2,1,2);
F2 = fft(ECG_final);
F2 = abs(F2);
F2 = F2(1:ceil(end/2));
F2 = F2/max(F2);

L= length(F2); 
f2= (1:1:L)*((Fs/2)/L); %Vector de frecuencia
plot(f2,F2);
ylabel('Magnitud normalizada');
xlabel('Frecuencia (Hz)'); title('ECG Sin Filtrar');

%% FINDPEAKS

% Establecer umbral para eliminar picos

umbral_y = 3*mean(abs(ECG_final));
umbral_x = 0.3*Fs; % 60/200 consideramos latido normal

[pksr,locs_Rwave] = findpeaks(ECG_final,'MinPeakHeight',umbral_y,'MinPeakDistance',umbral_x);
[pkss,locs_Swave] = findpeaks(-ECG_final,'MinPeakHeight',umbral_y,'MinPeakDistance',umbral_x);


figure;
hold on; 
plot(t,ECG_final);
scatter(t(locs_Rwave),pksr,'magenta');
scatter(t(locs_Swave),-pkss,'green');
grid on;
ylabel('Amplitud (mV)');
xlabel('Tiempo (s)');
title('R wave and S wave in Noisy ECG Signal');
legend('ECG','peaks_Rwave','peaks_Swave','Location','NorthWest');
xlim([0 10]);
hold off;

%% --- FILTRADO ---

%{
El objetivo que persigue el filtrado es la eliminación del ruido manteniendo 
las características de la señal.  
%}

%% LOW-PASS FILTERING - FILTRO BUTTERWORTH

%{
Esta técnica se basa en el diseño de un filtro paso bajo el cual 
elimina componentes frecuenciales altas, que generalmente se corresponden 
con señales interferentes en la captación de la señal (EMG). 
Para ello, la función MATLAB que se encarga de parametrizar el filtro es:

fdesign.lowpass(Fp, Fst, Ap, Ast, x, y, t, m)
%}

% All frequency values are in Hz.
Fs = 250;  % Sampling Frequency

Fp=0.2;     %Fpass
Fst=0.4;    %Fstop
Ap=0.1;     %Apass
Ast=80;     %Astop

% Construct an FDESIGN object and call its BUTTER method.
h  = fdesign.lowpass('Fp,Fst,Ap,Ast', Fp,Fst,Ap,Ast);
Hd = design(h, 'butter');

% Crear la señal filtada

ECG_limpia = filter(Hd,ECG_final);

figure;
plot(t,ECG_limpia); xlim([0 10]); title('ECG Filtrada con Butterworth'); 
ylabel('Amplitud (mV)');
xlabel('Tiempo (s)');

%% POWERLINE INTERFERENCE FILTERING - FILTRO FIR

%{
Con el objetivo de eliminar la interferencia de frecuencia de alimentación,
el uso de filtros FIR puede obtener un buen efecto de filtrado
%}

% Características del filtro
orden=200;
finf=30;
fsup=60;

% Normalizar frecuencias
finor = finf/(Fs/2);
fsnor = fsup/(Fs/2);

% Crear el filtro
a=1;
b=fir1(orden,[finor,fsnor],'stop');

% Filtrar la señal
ecg_filt = filtfilt(b,a,ECG_final);

figure;
subplot(2,1,1);
plot(t,ecg_filt); xlim([0 10]); title('ECG Filtro FIR'); ylabel('Amplitud (mV)');
xlabel('Tiempo (s)');

subplot(2,1,2);
F2 = fft(ecg_filt);
F2 = abs(F2);
F2 = F2(1:ceil(end/2));
F2 = F2/max(F2);

L= length(F2); 
f2= (1:1:L)*((Fs/2)/L); %Vector de frecuencia
plot(f2,F2);
ylabel('Magnitud normalizada');
xlabel('Frecuencia (Hz)'); title('ECG Filtro FIR');

%% BASELINE WANDER AND OFFSET REMOVAL 

%{
 IIR filtro digital de cambio de fase cero para corregir la deriva de 
 la línea de base 
%}

Wp = 1.4 * 2 / Fs; % frecuencia de corte de banda de paso 
Ws = 0.6 * 2 / Fs; % de frecuencia de corte de banda de parada 
Rp = 1; % coeficiente de ondulación de banda de paso  
Rs = 20; % de atenuación de banda de parada 

[N,Wn] = ellipord(Wp, Ws, Rp, Rs,'s'); % Encuentra el orden del filtro elíptico 
[b, a] = ellip(N, Rp, Rs, Wn,'high'); % Encuentra el coeficiente del filtro elíptico 
[hw,w]=freqz(b,a,512);   
result =filter(b,a,ecg_filt); 

figure;
freqz(b,a);
figure
subplot(211); plot(t,ecg_filt); 
 xlabel ('Tiempo (s)');  ylabel ('Amplitud (mV)'); title('Señal original'); grid on;
subplot(212); plot(t,result); 
 xlabel ('Tiempo (s)'); ylabel ('Amplitud (mV)');  title('Señal filtrada lineal');  grid on;

figure;
N=512;
subplot(2,1,1);plot(abs(fft(ecg_filt))*2/N);
 xlabel ('Frecuencia (Hz)'); ylabel ('Amplitud (mV)'); title('Espectro de señal original'); grid on;
subplot(2,1,2); plot(abs(fft(result))*2/N);
 xlabel ('Frecuencia (Hz)'); ylabel ('Amplitud (mV)'); title ('Después del filtrado lineal'); grid on;
subplot(2,1,2); plot(abs(fft(result))*2/N);
 xlabel ('Espectro de señal lineal filtrada'); ylabel ('Amplitud (mV)'); grid on;

 %% DETECT IF A GIVEN CHANNEL IS NOT CONNECTED
